package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

// Main function of the application
func main() {
	// Set up router and routes
	router := mux.NewRouter().StrictSlash(true)

	router.HandleFunc("/hello", helloHandle).Methods("GET")

	// CORS
	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"})
	originsOk := handlers.AllowedOrigins([]string{"*"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS", "DELETE"})

	fmt.Println("Server is running on port: 8001")

	// Start server
	log.Fatal(http.ListenAndServe(":8001", handlers.CORS(originsOk, headersOk, methodsOk)(router)))
}

func helloHandle(w http.ResponseWriter, r *http.Request) {
	type RestAPIResponse struct {
		Message string `json:"message"`
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	w.WriteHeader(200)

	returnResponse := RestAPIResponse{
		Message: "Hello world!",
	}

	json.NewEncoder(w).Encode(returnResponse)
}
